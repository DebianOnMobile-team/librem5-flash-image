#!/usr/bin/make

PREFIX ?= /usr/local

all: check

check:
	$(MAKE) -C scripts/
	$(MAKE) -C data/

install:
	$(MAKE) -C scripts/ install PREFIX=$(PREFIX)
	$(MAKE) -C data/ install PREFIX=$(PREFIX)
