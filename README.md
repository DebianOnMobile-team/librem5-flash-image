Librem5 Flash Image
===================

This repository contains the flashing script  for the Librem5
and the Librem5 devkit. If you just want to flash a new image check the
[official documentation][1].

[1]: https://developer.puri.sm/Librem5/Development_Environment/Boards.html
